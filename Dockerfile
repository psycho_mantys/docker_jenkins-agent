# syntax = docker/dockerfile:1

FROM jenkins/agent

USER root

ARG TARGETARCH
ARG TARGETOS
ARG TARGETPLATFORM
ARG SLIRP4NETNS_VERSION="1.2.2"

ARG DOCKER_COMPOSE_VERSION="1.29.2"

ENV DOCKER_HOST="unix:///home/jenkins/docker.sock"

ENV PATH="${PATH}:/usr/sbin:/sbin"

ARG APT_INSTALL="docker-ce docker-ce-cli containerd.io bash \
  openssh-client unzip pixz jq git docker-ce-rootless-extras \
  uidmap fuse-overlayfs iproute2 tor deb.torproject.org-keyring \
  rsync git-ftp wget curl gettext-base libcap2-bin shellcheck bc"

ENV XDG_RUNTIME_DIR=/home/jenkins/

ENV DOCKER_SOCKET_ADDR="unix:///home/jenkins/docker.sock" \
  DOCKER_COMPOSE_VERSION=${DOCKER_COMPOSE_VERSION}

RUN set -xv && apt-get clean && \
  apt-get --allow-unauthenticated --allow-insecure-repositories update && \
  apt-get --allow-unauthenticated install -y apt-transport-https \
  ca-certificates curl gnupg2 \
  software-properties-common && \
  install -m 0755 -d /etc/apt/keyrings && \
  curl -fsSL https://download.docker.com/linux/debian/gpg | gpg --dearmor -o /etc/apt/keyrings/docker.gpg && \
  chmod a+r /etc/apt/keyrings/docker.gpg && \
  #curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add - && \
  #apt-key fingerprint 0EBFCD88 && \
  echo \
  "deb [arch=$TARGETARCH signed-by=/etc/apt/keyrings/docker.gpg] https://download.docker.com/linux/debian $(lsb_release -cs) stable" > /etc/apt/sources.list.d/docker.list && \
  echo "deb [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org `dpkg --status tzdata|grep Provides|cut -f2 -d'-'` main" >> /etc/apt/sources.list.d/tor.list && \
  echo "deb-src [signed-by=/usr/share/keyrings/tor-archive-keyring.gpg] https://deb.torproject.org/torproject.org `dpkg --status tzdata|grep Provides|cut -f2 -d'-'` main" >> /etc/apt/sources.list.d/tor.list && \
  curl -fsSL https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --dearmor | tee /usr/share/keyrings/tor-archive-keyring.gpg >/dev/null && \
  apt-get --allow-insecure-repositories update && \
  apt-get --allow-unauthenticated install -y ${APT_INSTALL} && \
  curl -LO "https://dl.k8s.io/release/$(curl -L -s https://dl.k8s.io/release/stable.txt)/bin/${TARGETOS}/${TARGETARCH}/kubectl" && \
  install -o root -g root -m 0755 kubectl /usr/bin/kubectl && rm -rf kubectl && \
  curl -fL -o /usr/bin/slirp4netns https://github.com/rootless-containers/slirp4netns/releases/download/v${SLIRP4NETNS_VERSION}/slirp4netns-$(uname -m) && \
  chmod 755 /usr/bin/slirp4netns && \
  chmod u+s /usr/bin/newuidmap /usr/bin/newgidmap && \
  echo jenkins:100000:65536 | tee /etc/subuid | tee /etc/subgid && \
  curl -L "https://github.com/docker/compose/releases/download/${DOCKER_COMPOSE_VERSION}/docker-compose-$(uname -s)-$(uname -m)" -o /usr/bin/docker-compose && \
  chmod +x /usr/bin/docker-compose && \
  curl -fL "https://github.com/mikefarah/yq/releases/latest/download/yq_${TARGETOS}_${TARGETARCH}" -o /usr/bin/yq && \
  chmod +x /usr/bin/yq && \
  rm -rf /usr/share/man \
  && apt-get clean \
  && rm -rf /var/lib/apt/lists/*

COPY fuse-overlayfs-x86_64 /usr/bin/fuse-overlayfs
COPY docker-entrypoint.sh /
COPY --from=mvdan/shfmt /bin/shfmt /bin/

ENTRYPOINT ["/docker-entrypoint.sh"]

USER jenkins

#RUN mkdir -p ${HOME}/.config/docker/ && echo '{"storage-driver": "fuse-overlayfs"}' > ${HOME}/.config/docker/daemon.json

