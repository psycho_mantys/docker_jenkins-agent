#!/bin/bash

DOCKERD_EXEC_ARGS=${DOCKERD_EXEC_ARGS-"--exec-opt native.cgroupdriver=cgroupfs -H ${DOCKER_SOCKET_ADDR} --tls=false"}

if [ "${NATIVE_FUSE_OVERLAYFS}" != "false" ] ; then
	mkdir -p "${HOME}/.config/docker/" && \
		echo '{"storage-driver": "fuse-overlayfs"}' > "${HOME}/.config/docker/daemon.json"
fi

if [ "${DEBUG}" = "true" ] ; then
	export DOCKER_LOG_FILE=${DOCKER_LOG_FILE:-"/tmp/docker-rootless.log"}
else
	export DOCKER_LOG_FILE=${DOCKER_LOG_FILE:-"/dev/null"}
fi


if [ "${DOCKER_INIT}" != "false" ] ; then
	until docker container ls &> /dev/null ; do
		sleep 3
		# shellcheck disable=SC2086
		dockerd-rootless.sh ${DOCKERD_EXEC_ARGS} &> "${DOCKER_LOG_FILE}" &
	done
fi

exec "$@"

