![Docker Pulls](https://img.shields.io/docker/pulls/psychomantys/jenkins-agent)

# Jenkins agent to run on docker slaves env

Installed programs:

* xz
* git
* bzip2
* docker-cli
* docker-compose (The good version: **1.29.2**)
* kubectl
* bash
* openssh-client (ssh-agent)
* unzip
* pixz
* tor (for torify, get from torproject.org deb repo)
* jq
* rsync
* git-ftp
* gettext-base
* bc

